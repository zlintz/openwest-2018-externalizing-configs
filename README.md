# Externalizing Your Applications Configuration

One challenge in moving to a micro-service architecture, particularly one that leverages Docker, is configuration and secrets. 

Our apps need configuration in order to run. Not just one configuration, they need a different 
config per environment with different secrets. After all you don't want to point your dev environment at the production 
database right? Whether that is locally during development, deployed to a staging environment, or
all the way to production we need to give our applications what they need in order to run in that environment. 

Information in the session would include...

1. where to put configuration and secrets
2. give your app the fishing pole and an address so it can get the fish itself, I mean config... 
3. stacking default, external, and environment values to build out your configurations - high level with a node specific example
4. validating the config
5. the freedom externalizing your configuration gives you


_This session would focus on externalizing configuration using Consul and Vault_
_Tooling is language agnostic but any implementation examples to Consul/Vault would be presented nodejs_
