const Consul = require('../consul')
const Vault = require('../vault')
const convict = require('convict')
const _ = require('lodash')
const startupSchema = {
  demo: {
    doc: 'Is this a demo?',
    format: 'Boolean',
    default: true,
    env: 'DEMO',
    arg: 'DEMO'
  },
  consul: {
    host: {
      doc: 'Consul address',
      format: 'String',
      default: 'consul',
      env: 'CONSUL',
      arg: 'CONSUL'
    },
    port: {
      doc: 'Consul port',
      format: 'port',
      default: 8500,
      env: 'CONSUL_PORT',
      arg: 'CONSUL_PORT'
    }
  },
  vault: {
    token: {
      doc: 'Vault Token',
      format: 'String',
      default: null,
      env: 'VAULT_TOKEN',
      arg: 'VAULT_TOKEN'
    }
  }
}

function overrideParams (param) {
  return {env: param, arg: param}
}

const appConfigSchema = {
  granny: {
    grandson: {
      statement: {default: 'Why do you hate me so much???', format: 'String', ...overrideParams('GRANDSON_STATEMENT')}
    },
    response: {default: 'I just think you suck', format: 'String', ...overrideParams('GRANNY_STATEMENT')},
    pie: {
      ingredients: {default: ['apple', 'sugar'], format: 'Array', ...overrideParams('PIE_INGREDIENTs')},
      status: {default: 'inactive', format: 'String', ...overrideParams('PIE_STATUS')},
      temp: {default: 400, format: 'Number', ...overrideParams('PIE_TEMP')}
    },
    'passive-aggressive': {
      response: {default: 'No!', format: 'String', ...overrideParams('GRANNY_PA_RESPONSE')}
    },
    extra_non_consul_value: {default: 'pernonin noobs', format: 'String', ...overrideParams('EXTRA')}
  },
  secrets: {
    'my-secret': {default: 's3cr3t', format: 'String', ...overrideParams('SECRETS_MY_SECRET')},
    secret_number: {default: 0, format: 'Number', ...overrideParams('SECRETS_SECRET_NUMBER')},
    list_o_secrets: {default: 's3cr3t', format: 'Array', ...overrideParams('LIST_O_SECRETS')}
  }
}

const convictedStartupSchema = convict(startupSchema)
const convictedAppSchema = convict(appConfigSchema)




const PREFIX = 'granny'
const seedData = {
  [`${PREFIX}/grandson/statement`]: 'Grannies are for pwning noobs!',
  [`${PREFIX}/response`]: 'Grannies are for pwning noobs!',
  [`${PREFIX}/pie`]: {
    extra_request_only_in_consul: 'Make me a pie!',
    status: 'preheating',
    temp: 425,
    ingredients: ['strawberries', 'sugar']
  },
  [`${PREFIX}/passive-aggressive/response`]: 'I hope you like it, its strawberry!'
}

const seedSecrets = {
  'my-secret': 'super-s3cret',
  secret_number: 42,
  list_o_secrets: [1, 2, 3]
}

async function getVaultAddress(consul) {
  let ipAddress = (await consul.serviceInfo('vault'))[0].ServiceAddress
  let vaultAddress = `http://${ipAddress}:8200`
  if (convictedStartupSchema.get('demo')) {
    console.log(`Using vault enpdpoint of http://127.0.0.1:8200 for demo instead of service address of: ${vaultAddress}`)
    vaultAddress = 'http://127.0.0.1:8200'
  }
  return vaultAddress
}

async function demo() {
  const consul = new Consul()

  let vaultAddress = await getVaultAddress(consul)
  const vault = new Vault({token: convictedStartupSchema.get('vault.token'), endpoint: vaultAddress})

  await consul.storeSeedData(seedData)
  await vault.storeSeedData(seedSecrets)

  const secretPaths = Object.keys(seedSecrets)
  const secrets = await vault.getSecretsAndCombine(secretPaths)

  const consulConfig = await consul.getPrefixedKeysAndCombine(PREFIX)

  const combinedConfig = _.merge({}, consulConfig, {secrets})

  const stackedConfig = convictedAppSchema.load(combinedConfig).get()

  console.log('config: \n', JSON.stringify(stackedConfig, null, 2))

  vault.setSecretsToNonEnumerable(stackedConfig.secrets)
  console.log('protected config: \n', JSON.stringify(stackedConfig, null, 2))

  console.log('Secrets still accessible such as -> secret_number=', stackedConfig.secrets.secret_number)
}

demo()
