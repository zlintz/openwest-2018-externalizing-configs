const convict = require('convict')

process.env.VAULT_TOKEN = 'apples'

const schema = {
  consul: {
    host: {
      doc: 'Consul address',
      format: 'String',
      default: 'consul',
      env: 'CONSUL',
      arg: 'CONSUL'
    },
    port: {
      doc: 'Consul port',
      format: 'port',
      default: 8500,
      env: 'CONSUL_PORT',
      arg: 'CONSUL_PORT'
    }
  },
  vault: {
    token: {
      doc: 'Vault Token',
      format: 'String',
      default: null
      // env: 'VAULT_TOKEN',
      // arg: 'VAULT_TOKEN'
    }
  }
}

function applyConfigSchema (schema, config) {
  const convictedConfig = convict(schema)
  convictedConfig.load(config)
  convictedConfig.validate({allowed: 'strict'})
  return convictedConfig.get()
}

const externallyLoadedConfig = {
  consul: {
    host: '127.0.0.1'
  }
}

const convictedConfig = applyConfigSchema(schema, externallyLoadedConfig)
console.log('convictedConfig: ', JSON.stringify(convictedConfig, null, 2))
