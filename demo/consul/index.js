const consul = require('consul')
const _ = require('lodash')

function Consul (options = {}) {
  if (!(this instanceof Consul)) {
    return new Consul(options)
  }
  this._consul = consul({
    ...options,
    promisify: true
  })
}

Consul.prototype.store = async function (path, data) {
  this._consul.kv.set(path, JSON.stringify(data))
}

Consul.prototype.storeSeedData = async function storeSeedData(seedData) {
  return Promise.all(Object.keys(seedData).map(path => {
    return this.store(path, seedData[path])
  }))
}

Consul.prototype.getPrefixedKeys = async function (prefix) {
  return this._consul.kv.get({key: prefix, recurse: true})
}

Consul.prototype.combine = function (consulResults) {
  return consulResults.reduce((agg, kv) => {
    return _.set(agg, kv.Key.split('/').join('.'), JSON.parse(kv.Value))
  }, {})
}

Consul.prototype.getPrefixedKeysAndCombine = async function (prefix) {
  const prefixedKeys = await this.getPrefixedKeys(prefix)
  return this.combine(prefixedKeys)
}

Consul.prototype.deletePrefixedKeys = async function (prefix) {
  return this._consul.kv.del({key: prefix, recurse: true})
}

Consul.prototype.serviceInfo = async function (serviceName) {
  return this._consul.catalog.service.nodes({service: serviceName})
}

Consul.prototype.serviceHealth = async function (serviceName) {
  return this._consul.health.checks({service: serviceName})
}

Consul.prototype.servicesList = async function () {
  return this._consul.catalog.service.list()
}

module.exports = Consul
