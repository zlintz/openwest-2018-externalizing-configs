let consul = require('consul')({promisify: true})

async function storeAndRetrieve() {
  await consul.kv.set('hello', JSON.stringify('world'))

  const result = await consul.kv.get('hello')
  console.log('Result:', JSON.stringify(result, null, 2))

  await consul.kv.del('hello')
  const postDeleteResult = await consul.kv.get('hello')
  console.log('Post delete/missing key result: ', postDeleteResult)
}

storeAndRetrieve()
