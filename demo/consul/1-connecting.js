let consul = require('consul')({
  // host: '127.0.0.1', // default
  // port: 8500, // default
  promisify: true // default: false
})

consul.health.state('any', (err, result) => {
  if (err) {
    throw err
  }
  console.log('consul result: ', result)
})
