process.env.DEBUG = 'node-vault'
const token = require('./init_token').root_token

const vault = require('node-vault')({token: token})

async function getSecret (name) {
  try {
    const result = await vault.read(`secret/${name}`)
    return {key: name, value: result.data.value}
  } catch (err) {
    console.log({err: err}, `Error on secret with name: ${name}`)
    throw err
  }
}

async function writeSecret (name, value) {
  await vault.write(`secret/${name}`, {value})
}

async function start () {
  await writeSecret('hello', 'world')

  const helloValue = await getSecret('hello')
  console.log('secret/hello: ', helloValue)

  // await vault.delete('secret/hello')

  await getSecret('foo')
    .catch((_err) => {
      console.log('Foo key threw error')
    })
}

start()
