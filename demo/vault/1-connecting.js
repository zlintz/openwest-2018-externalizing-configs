// Modify version of the example in docs...
// https://github.com/kr1sp1n/node-vault/blob/master/example/write_read_delete.js
process.env.DEBUG = 'node-vault' // switch on debug mode

const config = {
  token: process.env.VAULT_TOKEN
}
// client.apiVersion = config.apiVersion || 'v1'
// client.endpoint = config.endpoint || process.env.VAULT_ADDR || 'http://127.0.0.1:8200'
// client.token = config.token || process.env.VAULT_TOKEN
const vault = require('node-vault')(config)

vault.write('secret/hello', {value: 'world', lease: '1s'})
  .then(() => vault.read('secret/hello'))
  .then(() => vault.delete('secret/hello'))
  .catch((err) => console.error(err.message))
